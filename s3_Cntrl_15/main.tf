provider "aws" {
  region = var.aws_region
} 

/* Senarios :
1. Public bucket without encryption
2. Internal bucket with default encryption key
3. Internal bucket with KMS encryption key
4. Restricted bucket with KMS encryption key
5. Highly restricted bucket with KMS encryption key
*/

# Public bucket without encryption
resource "aws_s3_bucket" "s15_cntrl_public_bucket_with_default_encryption" {
  bucket = var.bucket_name
  acl    = var.bucket_acl 
 
  versioning {
    enabled = false
  }
  
   tags = {
    dataclassification   = "Public"
    } 

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
        sse_algorithm     = "AES256"
        }
      }
    }
  
}
resource "aws_s3_bucket_policy" "s15_cntrl_public_bucket_with_default_encryption" {
  bucket = aws_s3_bucket.s15_cntrl_public_bucket_with_default_encryption.id
  policy = data.aws_iam_policy_document.s15_cntrl_public_bucket_with_default_encryption.json
}

data "aws_iam_policy_document" "s15_cntrl_public_bucket_with_default_encryption" {

 statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:Put*",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.s15_cntrl_public_bucket_with_default_encryption.id}",
    ]
    condition {
      test = "Bool"
      variable = "aws:SecureTransport"
      values = ["false"]
    }
  }
}

# Internal bucket with default encryption key

resource "aws_s3_bucket" "s15_cntrl_internal_bucket_with_default_encryption" {
  bucket = var.bucket_name
  acl    = var.bucket_acl 

  versioning {
    enabled = false
  }

  tags = {
    dataclassification   = "Internal"
  }
  
# Default Encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
              sse_algorithm     = "AES256"
        }
     }
  }
}

resource "aws_s3_bucket_policy" "s15_cntrl_internal_bucket_with_default_encryption_policy" {
  bucket = aws_s3_bucket.s15_cntrl_internal_bucket_with_default_encryption.id
  policy = data.aws_iam_policy_document.s15_cntrl_internal_bucket_with_default_encryption.json
}

data "aws_iam_policy_document" "s15_cntrl_internal_bucket_with_default_encryption" {

 statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.s15_cntrl_internal_bucket_with_default_encryption.id}",
    ]
    condition {
      test = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values = ["true"]
    }
  }
}
# Internal bucket with KMS encryption key

resource "aws_s3_bucket" "s15_cntrl_internal_bucket_with_kms_encryption" {
  bucket = var.bucket_name
  acl    = var.bucket_acl 

  versioning {
    enabled = false
  }

  tags = {
    dataclassification   = "Internal"
  }
  
# Default Encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
              sse_algorithm     = "AES256"
      }
    }
    }
   
}

resource "aws_s3_bucket_policy" "s15_cntrl_internal_bucket_with_kms_encryption" {
  bucket = aws_s3_bucket.s15_cntrl_internal_bucket_with_kms_encryption.id
  policy = data.aws_iam_policy_document.s15_cntrl_internal_bucket_with_kms_encryption_policy.json
}

data "aws_iam_policy_document" "s15_cntrl_internal_bucket_with_kms_encryption_policy" {

 statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.s15_cntrl_internal_bucket_with_kms_encryption.id}",
    ]
    condition {
      test = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values = ["true"]
    }
  }
statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.s15_cntrl_internal_bucket_with_kms_encryption.id}",
    ]
    condition {
      test = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption-aws-kms-key-id"
      values = ["arn:aws:kms:eu:west:2:${local.crypto_account}:key/aws"]
    }
  }
}
# Restricted bucket with KMS encryption key

resource "aws_s3_bucket" "s15_cntrl_restricted_bucket_with_kms_encryption" {
  bucket = var.bucket_name
  acl    = var.bucket_acl 

  versioning {
    enabled = false
  }

  tags = {
    dataclassification   = " Restricted"
  }
  
# Default Encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
              sse_algorithm     = "AES256"
      }
    }
    }
   
}

resource "aws_s3_bucket_policy" "s15_cntrl_restricted_bucket_with_kms_encryption" {
  bucket = aws_s3_bucket.s15_cntrl_restricted_bucket_with_kms_encryption.id
  policy = data.aws_iam_policy_document.s15_cntrl_restricted_bucket_with_kms_encryption_policy.json
}

data "aws_iam_policy_document" "s15_cntrl_restricted_bucket_with_kms_encryption_policy" {

 statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.s15_cntrl_restricted_bucket_with_kms_encryption.id}",
    ]
    condition {
      test = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values = ["true"]
    }
  }
statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.s15_cntrl_restricted_bucket_with_kms_encryption.id}",
    ]
    condition {
      test = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption-aws-kms-key-id"
      values = ["arn:aws:kms:eu:west:2:${local.crypto_account}:key/aws"]
    }
  }
}

# Highly Restricted bucket with KMS encryption key

resource "aws_s3_bucket" "s15_cntrl_highly_restricted_bucket_with_kms_encryption" {
  bucket = var.bucket_name
  acl    = var.bucket_acl 

  versioning {
    enabled = false
  }

  tags = {
    dataclassification   = " Highly Restricted"
  }
  
# Default Encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
              sse_algorithm     = "AES256"
      }
    }
    }
   
}

resource "aws_s3_bucket_policy" "s15_cntrl_highly_restricted_bucket_with_kms_encryption" {
  bucket = aws_s3_bucket.s15_cntrl_highly_restricted_bucket_with_kms_encryption.id
  policy = data.aws_iam_policy_document.s15_cntrl_highly_restricted_bucket_with_kms_encryption_policy.json
}

data "aws_iam_policy_document" "s15_cntrl_highly_restricted_bucket_with_kms_encryption_policy" {

 statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.s15_cntrl_highly_restricted_bucket_with_kms_encryption.id}",
    ]
    condition {
      test = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values = ["true"]
    }
  }
statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.s15_cntrl_highly_restricted_bucket_with_kms_encryption.id}",
    ]
    condition {
      test = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption-aws-kms-key-id"
      values = ["arn:aws:kms:eu:west:2:${local.crypto_account}:key/aws"]
    }
  }
}