variable aws_region {
     description = "Aws Region Name"
     type = string
     default = "eu-west-2"
 }
variable bucket_acl {
     description = "Acl for the S3 bucket: private,public-read ,public-read-write etc"
     type = string
     default = "private"
 }

#  variable bucket_name {
#      description = "Name of the bucket"
#      default = "bucket-0"
#  }
 
variable "ip_addresses" {
  description = "list of prohibited IP address"
  default = ["1.1.1.1"]
}

variable "s3_vpce_id" {
  description = "S3 VPC endpoint"
  default = ""
}