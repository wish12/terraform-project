provider "aws" {
  region = var.aws_region
} 

resource "aws_s3_bucket" "s26_cntrl_non_compliance_account" {
  bucket = "${local.bucket_name}-non-compliance-account"
   acl    = var.bucket_acl
  
 
  versioning {
    enabled = false
  }
  
   tags = {
    dataclassification   = "Internal"
    } 
}
resource "aws_s3_bucket_policy" "s26_cntrl_non_compliance_account" {
  bucket = aws_s3_bucket.s26_cntrl_non_compliance_account.id
  policy = data.aws_iam_policy_document.s26_cntrl_non_compliance_account.json
}

data "aws_iam_policy_document" "s26_cntrl_non_compliance_account" {

 statement {
    # sid = "  "
    effect = "Deny"
    principals {
      type = "AWS"
      identifiers = ["*"]
    }
    actions = [
      "s3:GetBucketPolicy",
    ]
    resources = [
         "arn:aws:s3:::${local.bucket_name}-non-compliance-account",
      "arn:aws:s3:::${local.bucket_name}-non-compliance-account/*"
    ]   
  }
}

