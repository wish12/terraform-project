provider "aws" {
  region = var.aws_region
} 
resource "aws_s3_bucket" "bucket_with_public_tag" {
  bucket = var.bucket_name
  acl    = var.bucket_acl 
  versioning {
  enabled = false
  }
  
   policy = <<POLICY
  {
   "Version" : "2012-10-17" ,
    "Statement" : [
      {
        "Sid"  : "PublicRead",
        "Effect" : "Allow",
        "Principal": "*",
        "Action ": ["s3:GetObject","s3:GetObjectVersion"],
        "Resource":["arn:aws:s3:::*"]
        
          }
     ]
      }
      POLICY

    server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
        // sse_algorithm     = "aws:kms"
       sse_algorithm     = "AES256"
        }
      }
    }
    tags = {
    dataclassification   = "Public"
    }
}

resource "aws_s3_bucket" "bucket_with_valid_internal_tag" {
  bucket = var.bucket_name
  acl    = var.bucket_acl 
  versioning {
    enabled = false
  }

  tags = {
    dataclassification   = "Internal"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
       // sse_algorithm     = "aws:kms"
       sse_algorithm     = "AES256"
      }
    }
    }
   policy = <<POLICY
 {
  "Id": "Policy1634220927174",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Default Encryption",
      "Effect": "Deny",
      "Principal": "*"
      "Action": ["s3:PutObject"], 
      "Resource": "arn:aws:s3:::${aws_s3_bucket.bucket_with_valid_restricted_tag.id}",
      "Condition": {
        "Null": {
          "s3:x-amz-server-side-encryption": "true"
        }
      },
      
    }
  ]
}
POLICY
}

resource "aws_s3_bucket_policy" "bucket_with_valid_internal_tag" {
  bucket = aws_s3_bucket.bucket_with_valid_internal_tag.id
  policy = data.aws_iam_policy_document.bucket_without_tls.json
}

data "aws_iam_policy_document" "bucket_without_tls" {

 statement {
    sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:ListBucket",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.bucket_with_valid_internal_tag.id}",
    ]
    condition {
      test = "Bool"
      variable = "aws:SecureTransport"
      values = ["false",]
    }
  }

  statement {
    sid = "Deny HTTP for object operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:GetObject",
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.bucket_with_valid_internal_tag.id}",
    ]
    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values = [
        "false",
      ]
    }
  }

}

resource "aws_s3_bucket" "bucket_with_valid_restricted_tag" {
   bucket = var.bucket_name
   acl    = var.bucket_acl   
   versioning {
    enabled = false
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.mykey.arn
        // sse_algorithm     = "aws:kms"
       sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
   dataclassification   =  "Restricted"
  }
}

resource "aws_s3_bucket_policy" "bucket_with_valid_restricted_tag" {
  bucket = aws_s3_bucket.bucket_with_valid_restricted_tag.id

 policy = <<POLICY
 {
  "Id": "Policy1634220927174",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Deny HTTP for object level operations",
      "Effect": "Deny",
       "Principal": "*"
      "Action": ["s3:PutObject" , "s3:GetObject",],
      "Resource": "arn:aws:s3:::${aws_s3_bucket.bucket_with_valid_restricted_tag.id}",
      "Condition": {
        "Bool": {"aws:SecureTransport": "false"}
      },
     
    },
    {
      "Sid": "Default Encryption",
      "Effect": "Deny",
      "Principal": "*"
      "Action": ["s3:PutObject"],
      "Resource": "arn:aws:s3:::${aws_s3_bucket.bucket_with_valid_restricted_tag.id}",
      "Condition": {
        "Null": {
          "s3:x-amz-server-side-encryption": "true"
        }
      },
      
    }
  ]
}
POLICY
}





