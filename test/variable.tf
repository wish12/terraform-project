variable aws_region {
     description = "Aws Region Name"
     type = string
     default = "eu-west-2"
 }
variable bucket_acl {
     description = "Acl for the S3 bucket: private,public-read ,public-read-write etc"
     type = string
     default = "private"
 }

 variable bucket_name {
     description = "Name of the bucket"
     default = "bucket-0"
 }

 resource "aws_kms_key" "mykey" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}