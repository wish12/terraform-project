provider "aws" {
  region = var.aws_region
} 
/* Senarios :
1. Encryption of data in transit must be enforced:
            Communication to s3 via Management console / Service APIs must be encrypted with TLS 1.2
*/
resource "aws_kms_key" "mykey" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}


# Public bucket without encryption
resource "aws_s3_bucket" "public_bucket_with_default_encryption" {
  bucket = "${local.bucket_name}-public"
  # acl    = var.bucket_acl
  acl    = "public-read"
 
  versioning {
    enabled = false
  }
  
   tags = {
    dataclassification   = "Public"
    } 

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
               sse_algorithm     = "AES256"
        }
      }
    }
  
}
resource "aws_s3_bucket_policy" "public_bucket_with_default_encryption" {
  bucket = aws_s3_bucket.public_bucket_with_default_encryption.id
  policy = data.aws_iam_policy_document.public_bucket_with_default_encryption.json
}

data "aws_iam_policy_document" "public_bucket_with_default_encryption" {

 statement {
    # sid = "Deny HTTP for bucket level operations and enforcing TLS version 1.2  "
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:Put*",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.public_bucket_with_default_encryption.id}/*",
    ]
    condition {
      test = "Bool"
      variable = "aws:SecureTransport"
      values = ["false"]
    }
    condition {
      test = "NumericLessThan"
      variable = "s3:TlsVersion"
      values = ["1.2"]
    }
  }

}

# Internal bucket with default encryption key

resource "aws_s3_bucket" "internal_bucket_with_default_encryption" {
 # bucket = var.bucket_name
 bucket="${local.bucket_name}-internal-1"
  acl    = var.bucket_acl 

  versioning {
    enabled = false
  }

  tags = {
    dataclassification   = "Internal"
  }
  
# Default SSE Encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
               sse_algorithm     = "AES256"
        }
     }
  }
}

resource "aws_s3_bucket_policy" "internal_bucket_with_default_encryption_policy" {
  bucket = aws_s3_bucket.internal_bucket_with_default_encryption.id
  policy = data.aws_iam_policy_document.internal_bucket_with_default_encryption.json
}

data "aws_iam_policy_document" "internal_bucket_with_default_encryption" {

statement {
    # sid = "Deny HTTP for bucket level operations and enforcing TLS version 1.2  "
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:Put*",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.internal_bucket_with_default_encryption.id}/*",
    ]
    condition {
      test = "Bool"
      variable = "aws:SecureTransport"
      values = ["false"]
    }
    condition {
      test = "NumericLessThan"
      variable = "s3:TlsVersion"
      values = ["1.2"]
    }
  }

 statement {
    # sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:Put*",
    ]
    # Another way to define the same thing
    #  resources = ["${aws_s3_bucket.internal_bucket_with_default_encryption.arn}/*"]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.internal_bucket_with_default_encryption.id}/*",
    ]
    
    condition {
      test = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values = ["true"]
    }
  }
}
# Internal bucket with KMS encryption key

resource "aws_s3_bucket" "internal_bucket_with_kms_encryption" {
 # bucket = var.bucket_name
  bucket="${local.bucket_name}-internal-2"
  acl    = var.bucket_acl 

  versioning {
    enabled = false
  }

  tags = {
    dataclassification   = "Internal"
  }
  
# Default SSE Encryption

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
          kms_master_key_id = aws_kms_key.mykey.arn
              sse_algorithm     = "aws:kms"
         
      }
    }
    }
   
}

resource "aws_s3_bucket_policy" "internal_bucket_with_kms_encryption" {
  bucket = aws_s3_bucket.internal_bucket_with_kms_encryption.id
  policy = data.aws_iam_policy_document.internal_bucket_with_kms_encryption_policy.json
}

data "aws_iam_policy_document" "internal_bucket_with_kms_encryption_policy" {

 statement {
  # sid = "Deny HTTP for bucket level operations and enforcing TLS version 1.2 "
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:Put*",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.internal_bucket_with_kms_encryption.id}/*",
    ]
    condition {
      test = "Bool"
      variable = "aws:SecureTransport"
      values = ["false"]
    }
    condition {
      test = "NumericLessThan"
      variable = "s3:TlsVersion"
      values = ["1.2"]
    }
  }

 statement {
    # sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:Put*",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.internal_bucket_with_kms_encryption.id}/*",
    ]
    condition {
      test = "Null"
      variable = "s3:x-amz-server-side-encryption"
      values = ["true"]
    }
  }
  

statement {
    # sid = "Deny HTTP for bucket level operations"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:Put*",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.internal_bucket_with_kms_encryption.id}/*",
    ]
    condition {
      test = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption-aws-kms-key-id"
      #values = ["arn:aws:kms:eu:west:2:${local.crypto_account}:key/aws"]
      values = ["arn:aws:kms:eu:west:2:${local.bucket_name}:key/aws"]
    }
  }
  statement {
    # sid = "Deny bucket access not through vpce"
    effect = "Deny"
    principals {
      type = "AWS"
      identifiers = ["*"]
    }
    actions = [
     # "s3:ListBucket",
      "s3:*",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.internal_bucket_with_kms_encryption.id}/*",
    ]
    # condition {
    #   test     = "NotIpAddress"
    #   variable = "aws:SourceIp"
    #   values = var.ip_addresses
    # }
    condition {
      test     = "StringNotEquals"
      variable = "aws:SourceVpce"
      values = [
        "vpce-111111",
        var.s3_vpce_id,
        ]
    }
  }

  # statement {
  #   # sid = "Deny object access not through vpce"
  #   effect = "Deny"
  #   principals {
  #     type = "AWS"
  #     identifiers = ["*"]
  #   }
  #   actions = [
  #     "s3:Get*",
  #     "s3:Put*"
  #   ]
  #   resources = [
  #     "arn:aws:s3:::${aws_s3_bucket.internal_bucket_with_kms_encryption.id}/*",
  #   ]
  #   condition {
  #     test     = "StringNotEquals"
  #     variable = "aws:SourceVpce"
  #     values = [
  #       "vpce-111111111111111111",
  #       var.s3_vpce_id,
  #     ]
  #   }
  # }
  
}
